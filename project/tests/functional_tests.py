from unittest.mock import patch

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

from project.tests import SeleniumTestCaseMixin
from project.webapp.models import Content
from project.webapp.tests import parsed_content


class AdminTest(StaticLiveServerTestCase, SeleniumTestCaseMixin):

    # load fixtures
    fixtures = ['auth.json', 'webapp.organisation_type.json', 'webapp.content.json']

    def setUp(self):
        # use phantomjs for speed, firefox to see vaguely what goes on
        self.browser = webdriver.PhantomJS()  # npm install -g phantomjs (need nodejs, npm)
        # self.browser = webdriver.Firefox()  #installed by default

    def tearDown(self):
        self.browser.quit()

    def login(self, username='admin', password='admin'):
        """
        login procedure in Selenium
        """
        # user types username, password and press ENTER
        username_field = self.browser.find_element_by_name('username')
        username_field.send_keys(username)
        password_field = self.browser.find_element_by_name('password')
        password_field.send_keys(password)
        password_field.send_keys(Keys.RETURN)

    def test_home_redirects_to_admin_site(self):
        # user goes to home and is redirected to admin
        self.open(reverse('admin:index'))

        # the page's body has class login
        try:
            body_el = self.browser.find_element_by_css_selector("body.login")
        except NoSuchElementException:
            body_el = None
        self.assertIsNotNone(body_el)

        # title and header contain the string
        self.assertIn('VerificaFonti', self.browser.title)
        self.assertIn('VerificaFonti', self.browser.page_source)

    def test_admin_user_can_login_into_admin_site(self):
        self.open(reverse('admin:index'))
        self.login()

        # login credentials are correct,
        # the user is redirected to the main admin page
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Amministrazione sito', body.text)

        # user clicks on the Users link
        user_link = self.browser.find_elements_by_link_text('Utenti')
        user_link[0].click()

        # user verifies that user live@forever.com is present
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('live@forever.com', body.text)

    def test_logged_user_can_create_content(self):
        self.open(reverse('admin:index'))
        self.login()

        # user clicks on the Contenuti link
        user_link = self.browser.find_elements_by_link_text('Contenuti')
        user_link[0].click()
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Scegli contenuto da modificare', body.text)

        # user clicks on the Aggiungi contenuto link
        # TODO: see why this does not work:
        # user_link = self.browser.find_elements_by_partial_link_text('Aggiungi contenuto')
        user_link = self.browser.find_elements_by_css_selector('.object-tools a.addlink')
        user_link[0].click()

        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Aggiungi contenuto', body.text)

        # user fills out the form
        title_field = self.browser.find_element_by_name('title')
        title_field.send_keys("Test content")

        # user finds the organisation type in the dropdown
        org_field = self.browser.find_element_by_name('organisation_type')
        for option in org_field.find_elements_by_tag_name('option'):
            if option.text == 'Giunta Comunale':
                option.click()

        # user clicks the save button before adding URL
        save_btn = self.browser.find_element_by_css_selector("input[value='Salva']")
        save_btn.click()

        url_error = self.browser.find_element_by_css_selector('.form-row.errors.field-url ul li')
        self.assertEqual('Questo campo è obbligatorio.', url_error.text)

        # adds url
        url_field = self.browser.find_element_by_name('url')
        url_field.send_keys('http://www.openpolis.it')

        # adds op_url
        op_url_field = self.browser.find_element_by_name('op_url')
        op_url_field.send_keys('http://politici.openpolis.it/regione/basilicata/19')

        # save again
        save_btn = self.browser.find_element_by_css_selector("input[value='Salva']")
        save_btn.click()

        # verify that the Content has been added
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Test content', body.text)

    @patch.object(Content, 'get_live_content')
    def test_logged_user_processes_content(self, mock_get_live_content):
        """
        The whole workflow of processing a content is tested,
        with all possible scenarios:
        - the user searches for a specific content (roma)
        - the user logs in and is redirected to the search results page
        - the results page contains the searched content
        - user clicks the first action button and the dropdown select appears
        - user clicks on the verify link and
          new status message informs that content is changed
        - user clicks link to diff viewer
        - newly open window's title contains the string
          "visualizzatore differenze"
        - diff view page contains both source and openpolis urls
        :param mock_get_live_content: the mocked Content.get_live_content()
        - div view page is closed
        - user clicks on the OP Aggiornato link and
          new status message informs that content is uptodate
        - user clicks on the Reset link and
          status message is reset
        :return: None
        """
        # configure the mock to return the parsed content
        mock_get_live_content.return_value = (200, parsed_content)

        content = Content.objects.get(title="GC - 5132 - Roma (RM)")

        # the user searches for a specific content (roma)
        self.open(reverse('admin:webapp_content_changelist') + "?q=roma")

        # the user logs in and is redirected to the search results page
        # the results page contains the searched content
        self.login()
        city_link = self.browser.find_element_by_partial_link_text(content.title)
        self.assertIsNotNone(city_link)

        # user clicks the first action button and the dropdown select appears
        actions_link = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td[@class="field-_row_actions"]/a'
        )
        actions_link.click()
        self.assertEqual('jq-dropdown-open', actions_link.get_attribute("class"))

        # user clicks on the verify link and new status messages informs that content is changed
        verify_link = actions_link.find_element_by_xpath(
            'following-sibling::div/ul/li/a'
        )
        self.assertEqual('Verifica contenuto', verify_link.text)
        verify_link.click()
        city_link = self.browser.find_element_by_partial_link_text(content.title)
        changed_status = Content.STATUS_CHANGED
        changed_status_label = dict(
            Content.STATUS_CHOICES
        )[Content.STATUS_CHANGED].upper()
        status_message = "{0} - {1}".format(
            changed_status, changed_status_label
        )
        status_td = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td[@class="field-_status_and_message"]'
        )
        self.assertIn(status_message, status_td.text)

        # keep windows information,
        # to test newsly open window or tabs
        current_windows = self.browser.window_handles
        main_window = current_windows[0]

        # user clicks link to diff viewer
        diff_link = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td['
            '@class="field-_status_and_message"]/a'
        )
        diff_link.click()

        # newly open diff view page's title contains the string
        # "visualizzatore differenze"
        new_windows = self.browser.window_handles
        new_window = list(set(new_windows) - set(current_windows))[0]
        self.browser.switch_to.window(new_window)
        self.assertIn("visualizzatore differenze", self.browser.title.lower())

        # diff view page contains both source and openpolis urls
        fonte_link = self.browser.find_element_by_partial_link_text("alla fonte")
        self.assertIn(fonte_link.get_attribute('href'), content.url)
        openpolis_link = self.browser.find_element_by_partial_link_text("alla pagina openpolis")
        self.assertIn(openpolis_link.get_attribute('href'), content.op_url)

        # close diff view window
        self.browser.close()

        self.browser.switch_to.window(main_window)

        # user clicks on the actions link
        # and dropdown select appears
        actions_link = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td[@class="field-_row_actions"]/a'
        )
        actions_link.click()
        self.assertEqual('jq-dropdown-open', actions_link.get_attribute("class"))

        # user clicks on the OP Aggiornato link and
        # new status messages informs that content is uptodate
        dropdown_div = actions_link.find_element_by_xpath(
            'following-sibling::div'
        )
        _link = dropdown_div.find_element_by_partial_link_text(
            'OP Aggiornato'
        )
        _link.click()
        city_link = self.browser.find_element_by_partial_link_text(content.title)
        status = Content.STATUS_UPDATED
        status_label = dict(
            Content.STATUS_CHOICES
        )[status].upper()
        status_message = "{0} - {1}".format(
            status, status_label
        )
        status_td = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td[@class="field-_status_and_message"]'
        )
        self.assertIn(status_message, status_td.text)

        # user clicks on the actions link
        # and dropdown select appears
        actions_link = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td[@class="field-_row_actions"]/a'
        )
        actions_link.click()
        self.assertEqual('jq-dropdown-open', actions_link.get_attribute("class"))

        # user clicks on the OP Aggiornato link and
        # new status messages informs that content is uptodate
        dropdown_div = actions_link.find_element_by_xpath(
            'following-sibling::div'
        )
        _link = dropdown_div.find_element_by_partial_link_text(
            'Reset'
        )
        _link.click()
        city_link = self.browser.find_element_by_partial_link_text(content.title)
        status = None
        status_label = None
        status_message = "{0} - {1}".format(
            status, status_label
        )
        status_td = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td[@class="field-_status_and_message"]'
        )
        self.assertIn(status_message, status_td.text)

        # user clicks on the actions link
        # and dropdown select appears
        actions_link = city_link.find_element_by_xpath(
            'parent::th/following-sibling::td[@class="field-_row_actions"]/a'
        )
        actions_link.click()
        self.assertEqual('jq-dropdown-open', actions_link.get_attribute("class"))

        # when the user clicks on the Segnala Errore link
        # the signal_error view page is shown, with
        # "Invio segnalazione di errore" in the title
        dropdown_div = actions_link.find_element_by_xpath(
            'following-sibling::div'
        )
        _link = dropdown_div.find_element_by_partial_link_text(
            'Segnala Errore'
        )
        _link.click()

        self.assertIn("invio segnalazione di errore", self.browser.title.lower())
