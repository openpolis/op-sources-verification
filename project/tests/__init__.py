class SeleniumTestCaseMixin(object):
    """
    A base test case for Selenium, providing hepler methods for generating
    clients and logging in profiles.

    Needs to be used in a class extending LiveServerTestCase,
    where self.browser has been defined
    """
    browser = None
    live_server_url = None

    def open(self, url):
        if self.browser is None:
            raise Exception("browser attribute must be defined in main class")
        if self.live_server_url is None:
            raise Exception("browser live_server_url must be defined in main class")

        self.browser.get("%s%s" % (self.live_server_url, url))
