import random
import re
import time
from typing import Optional

from constance import config
from playwright.sync_api import Browser
from playwright.sync_api import Error as PlaywrightError
from playwright.sync_api import Playwright, sync_playwright, TimeoutError as PlaywrightTimeoutError
from playwright_stealth import stealth_sync
from config.settings.base import USER_AGENTS


class PlaywrightWrapper:
    """Pooled playwright (https://playwright.dev) wrapper, that allows users to
    get live textual content and status from URLs, and selectors

    simple usage:

        p = PlaywrightWrapper(request_ua=REQUEST_UA)
        status, content = p.get_live_content(url, selector)

    Resources (Playwright instance, Browser, Context and Page are pooled.
    This drastically reduces the time it takes, avoiding the need to create Browser and/or Pages anew.
    """
    p: Playwright
    browser: Browser
    proxy: Optional[dict]
    request_timeout: int
    request_ua: str

    def __init__(
            self,
            proxy: Optional[dict] = None,
            request_ua: str = config.REQUESTS_UA,
            request_timeout_sec: int = config.REQUESTS_MAX_TIMEOUT
    ):
        self.p = sync_playwright().start()

        # if proxy was not passed among the arguments,
        # then check if it's in  the Constance's config
        self.proxy = proxy
        if proxy is None and config.PROXY_URL:
            self.proxy = {
                'url': config.PROXY_URL,
                'username': config.PROXY_USERNAME,
                'password': config.PROXY_PASSWORD
            }
        self.request_ua = request_ua
        self.request_timeout = request_timeout_sec * 1000
        self.browser = self.p.chromium.launch(**self.get_browser_args())
        self.brightd_sceaping_auth = config.AUTH_BRIGHT_DATA_SCRAPING

    def update_get_browser_args_for_captcha(self):
        print("Using captcha PROXY")
        self.browser.close()
        endpoint_url = f'wss://{self.brightd_sceaping_auth}@brd.superproxy.io:9222'
        self.browser = self.p.chromium.connect_over_cdp(endpoint_url)

    def get_browser_args(self):
        """Prepare browser args."""
        browser_args = {"headless": True,
                        "args": ["--disable-http2"]}

        proxy_configured = (
                self.proxy and 'url' in self.proxy and 'username' in self.proxy and 'password' in self.proxy
        )

        if proxy_configured:
            browser_args.update(
                {
                    "proxy": {
                        "server": self.proxy['url'],
                        "username": self.proxy['username'],
                        "password": self.proxy['password'],
                    }
                }
            )
        return browser_args

    def get_browser_context_args(self):
        viewport = {
            "width": random.randint(800, 1920),
            "height": random.randint(600, 1080)
        }
        """Prepare browser context args."""
        browser_args = {"ignore_https_errors": True,
                        "user_agent": random.choice(USER_AGENTS),
                        "viewport": viewport,
                        "java_script_enabled": True
                        }
        if self.request_ua:
            browser_args.update({"user_agent": self.request_ua})
        return browser_args

    # @property
    # def browser_and_context(self):
    #     browser: Browser = self.p.chromium.launch(**self.get_browser_args())
    #     context = browser.new_context(**self.get_browser_context_args())
    #     return browser, context

    def get_live_content(self, url, selector, **kwargs):
        """
        Requests content from URI, using playwright (https://playwright.dev/python/)

        Finds the content using the object's selector attribute
        (an XPATH or a CSS ore some othere locatos, as specified in https://playwright.dev/python/docs/locators).

        :return: 2-tuple
          the cleanest possible textual content, or a comprehensible error message,
          along with the response status code
        """
        selector = selector or "body"
        time_response_took = None
        time_sleep = kwargs.get('time_sleep', 0)
        has_captcha = False
        use_captcha_resolver = kwargs.get("use_captcha_resolver", False)
        wait_time = kwargs.get('wait_time', 0)
        context = self.browser.new_context(**self.get_browser_context_args())
        page = context.new_page()
        page.add_init_script("""

                    // Imposta una lingua comune
                    Object.defineProperty(navigator, 'languages', {
                        get: () => ['en-US', 'en']
                    });

                    // Simula un WebGL Renderer realistico
                    Object.defineProperty(WebGLRenderingContext.prototype, 'getParameter', {
                        value: (parameter) => {
                            if (parameter === 37445) return 'NVIDIA GeForce RTX 3080'; // GPU comune
                            if (parameter === 7937) return 'NVIDIA Corporation';
                            return WebGLRenderingContext.prototype.getParameter(parameter);
                        }
                    });

                    // Simula un plugin realistico
                    Object.defineProperty(navigator, 'plugins', {
                        get: () => [1, 2, 3, 4, 5]
                    });

                    // Simula un corretto deviceMemory
                    Object.defineProperty(navigator, 'deviceMemory', {
                        get: () => 8
                    });

                    // Aggiunge un controllo per i sensori di input
                    Object.defineProperty(navigator, 'maxTouchPoints', {
                        get: () => 1
                    });
                """)

        try:
            time_response_start = time.time()
            response = page.goto(url, wait_until="load", timeout=20_000)
            time.sleep(wait_time)
            time_response_took = time.time() - time_response_start
        except PlaywrightTimeoutError as e:
            status = 408
            content = str(e)
        except PlaywrightError as e:
            status = 990
            content = str(e)
        else:
            status = response.status
            if status in (200, 202):
                regex = r"(?:\n|\t|\xa0)+(?:\s+(?:\n|\t|\xa0)+)?"
                locator = page.locator(selector)

                try:
                    count = locator.count()
                except PlaywrightError as e:
                    status = 900
                    content = f"{e}"
                else:
                    if not count:
                        status = 900
                        content = "Selettore non trovato"

                    elif count > 1:
                        content = locator.all_inner_texts()
                        content = "\n".join(
                            re.sub(regex, "\n", x.strip("- \n\t")) for x in content
                        )
                    else:
                        content = locator.inner_text()
                        content = re.sub(regex, "\n", content.strip("- \n\t"))
            else:
                if status == 404:
                    content = "Pagina non trovata"
                else:
                    content = response.status_text

        if time_response_took:
            trt = f"{time_response_took:.03}s"
        else:
            trt = "-"

        try:
            if status in [408]:
                if use_captcha_resolver:
                    self.update_get_browser_args_for_captcha()
            elif (status in [403, 468, 990]
                  or (page and 'captcha' in page.content().lower() and status not in [200,
                                                                                      202]) or
                  (page and 'Radware Captcha Page'.lower() in page.content().lower())):
                if 'Radware Captcha Page'.lower() in page.content().lower():
                    has_captcha = True
                if use_captcha_resolver:
                    self.update_get_browser_args_for_captcha()
                # page.close()
        except Exception:
            pass
        else:
            try:
                page.close()
                context.close()
            except Exception:
                pass

        if status in [200, 202] and not has_captcha:
            error_msg = "-"
        else:
            if time_sleep < 10:
                time_sleep += 15
                return self.get_live_content_2(url, selector, time_sleep=time_sleep,
                                               slow_search=True, with_stealth=False)
            error_msg = content

        print(f"{url} - {selector} - {status} - {error_msg} - {trt}")

        return status, content

    def get_live_content_2(self, url, selector, **kwargs):
        """
        Requests content from URI, using playwright (https://playwright.dev/python/)

        Finds the content using the object's selector attribute
        (an XPATH or a CSS ore some othere locatos, as specified in https://playwright.dev/python/docs/locators).

        :return: 2-tuple
          the cleanest possible textual content, or a comprehensible error message,
          along with the response status code
        """
        with_stealth = kwargs.get("with_stealth", True)
        time_sleep = kwargs.get('time_sleep', 0)
        slow_search = kwargs.get('slow_search', False)
        selector = selector or "body"
        time_response_took = None

        context = self.browser.new_context(**self.get_browser_context_args())
        if slow_search:
            time.sleep(random.uniform(1, 3))
        page = context.new_page()
        if with_stealth:
            stealth_sync(page)
        try:
            time_response_start = time.time()
            # page.evaluate("navigator.webdriver = undefined")

            try:
                page.mouse.wheel(0, random.randint(200, 500))
                response = page.goto(url, wait_until="networkidle")
                if slow_search:
                    for i in range(5):  # Movimento del mouse in modo non lineare
                        x_offset = random.uniform(-50, 50)
                        y_offset = random.uniform(-30, 30)
                        page.mouse.move(x_offset, y_offset, steps=random.randint(5, 10))
                        time.sleep(random.uniform(0.5, 1.5))
                page.mouse.wheel(0, random.randint(200, 500))
                try:
                    page.query_selector("button.close").click(timeout=3)
                except Exception:
                    pass
                try:
                    page.query_selector("button.iubenda-cs-close-btn").click(timeout=3)
                except Exception:
                    pass
            except PlaywrightError:
                try:
                    response = page.goto(url, timeout=20_000)
                    time.sleep(time_sleep)
                    try:
                        page.query_selector("button.close").click(timeout=0.5)
                    except Exception:
                        pass
                    page.mouse.wheel(0, random.randint(200, 500))
                    time_response_took = time.time() - time_response_start
                except Exception as e:
                    raise e
            page.wait_for_load_state("domcontentloaded")

            time.sleep(time_sleep)
            time_response_took = time.time() - time_response_start
        except PlaywrightError as e:
            status = 990
            content = str(e)
        else:
            status = response.status
            if status in (200, 202, 468):
                regex = r"(?:\n|\t|\xa0)+(?:\s+(?:\n|\t|\xa0)+)?"
                locator = page.locator(selector)

                try:
                    count = locator.count()
                except PlaywrightError as e:
                    status = 900
                    content = f"{e}"
                else:
                    if not count:
                        status = 900
                        content = "Selettore non trovato"
                    elif count > 1:
                        status = 200
                        content = locator.all_inner_texts()
                        content = "\n".join(
                            re.sub(regex, "\n", x.strip("- \n\t")) for x in content
                        )
                    else:
                        status = 200
                        content = locator.inner_text()
                        content = re.sub(regex, "\n", content.strip("- \n\t"))
            else:
                if status == 404:
                    content = "Pagina non trovata"
                else:
                    content = response.status_text

        if time_response_took:
            trt = f"{time_response_took:.03}s"
        else:
            trt = "-"

        page.close()
        context.close()

        if status in [200, 202]:
            error_msg = "-"
        else:
            if time_sleep < 15:
                time_sleep += 5
                return self.get_live_content_2(url, selector, time_sleep=time_sleep,
                                               slow_search=True, with_stealth=not with_stealth)
            error_msg = content

        print(f"{url} - {selector} - {status} - {error_msg} - {trt}")

        return status, content

    def stop(self):
        self.browser.close()
        self.p.stop()
