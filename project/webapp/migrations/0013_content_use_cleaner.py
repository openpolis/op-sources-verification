# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-10-24 13:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0012_auto_20160919_1556'),
    ]

    operations = [
        migrations.AddField(
            model_name='content',
            name='use_cleaner',
            field=models.BooleanField(default=True, verbose_name='Utilizza cleaner (rimuovi scripts, css e commenti)'),
        ),
    ]
