# Generated by Django 4.2.17 on 2025-02-19 10:16

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("webapp", "0023_rename_xpath_content_selector_alter_content_id_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="content",
            name="next_content",
            field=models.TextField(
                blank=True, null=True, verbose_name="Contenuto significativo nuovo"
            ),
        ),
    ]
