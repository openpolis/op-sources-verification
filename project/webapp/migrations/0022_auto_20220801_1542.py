# Generated by Django 3.1.7 on 2022-08-01 13:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0021_content_use_proxy'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='use_cleaner',
            field=models.BooleanField(default=True, verbose_name='Utilizza cleaner'),
        ),
        migrations.AlterField(
            model_name='content',
            name='use_proxy',
            field=models.BooleanField(default=True, verbose_name='Utilizza proxy IT'),
        ),
    ]
