# Generated by Django 2.1.2 on 2019-05-07 11:49

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0013_content_use_cleaner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='organisation_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='webapp.OrganisationType', verbose_name='Tipo di organizzazione'),
        ),
    ]
