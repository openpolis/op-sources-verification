"""Webapp utils."""

import requests
from constance import config

requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL:@SECLEVEL=1'


def prepare_request(timeout=None, use_proxy=False):
    """Populate request session with config."""
    request = requests.Session()
    request.verify = False

    if timeout is not None:
        request.timeout = timeout
    else:
        request.timeout = config.REQUESTS_MAX_TIMEOUT or 10

    if config.REQUESTS_UA:
        request.headers.update({"User-Agent": config.REQUESTS_UA})

    if config.PROXY_URL and use_proxy:
        request.proxies = {
            "http": config.PROXY_URL,
            "https": config.PROXY_URL
        }
    return request
