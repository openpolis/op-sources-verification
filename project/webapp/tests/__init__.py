# get some big strings from files, once for every tests
with open('./resources/test/source_original.html', 'r') as src_f:
    html_content = src_f.read().encode('utf-8')
with open('./resources/test/source_parsed_content.txt', 'r') as src_f:
    parsed_content = src_f.read().strip()
