"""Tests for webapp utils."""

from constance.test import override_config
from django.test import SimpleTestCase

from project.webapp.utils import prepare_request


class UtilsTests(SimpleTestCase):
    """Utitls tests class."""

    def test_prepare_request_defaults(self):
        """Default from django settings."""
        request = prepare_request()
        self.assertEqual(request.timeout, 10)
        self.assertFalse(request.verify)
        self.assertEqual(request.proxies, {})
        self.assertEqual(request.headers["User-Agent"], "python-requests/2.25.1")

    @override_config(REQUESTS_MAX_TIMEOUT=300)
    @override_config(REQUESTS_UA="test/test")
    @override_config(PROXY_URL="http://test.com")
    def test_prepare_request_with_constance(self):
        """Config using Django constance."""
        request = prepare_request(use_proxy=True)
        self.assertEqual(request.timeout, 300)
        self.assertEqual(request.headers["User-Agent"], "test/test")
        self.assertEqual(
            request.proxies,
            {
                "http": "http://test.com",
                "https": "http://test.com",
            }
        )

    def test_prepare_request_timeout_parameter(self):
        """Override timeout via parameter."""
        request = prepare_request(timeout=200)
        self.assertEqual(request.timeout, 200)

    @override_config(PROXY_URL="http://test.com")
    def test_prepare_request_proxy_parameter(self):
        """Using proxy."""
        request = prepare_request(use_proxy=True)
        self.assertEqual(
            request.proxies,
            {
                "http": "http://test.com",
                "https": "http://test.com",
            }
        )

    @override_config(PROXY_URL="http://test.com")
    def test_prepare_request_no_proxy_parameter(self):
        """Not using proxy."""
        request = prepare_request()
        self.assertEqual(request.proxies, {})
