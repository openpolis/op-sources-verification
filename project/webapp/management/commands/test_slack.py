import slack
from constance import config
from django.core.management.base import BaseCommand

from project.webapp.models import Content


class Command(BaseCommand):
    help = 'Test slack message'

    def handle(self, *args, **options):

        @slack.RTMClient.run_on(event='message')
        def say_hello(**payload):

            data = payload['data']
            web_client = payload['web_client']
            _ = payload['rtm_client']
            if 'Hello' in data.get('text', None):
                channel_id = data['channel']
                thread_ts = data['ts']
                user = data.get('user', 'ND')
                n_content = Content.objects.count()
                web_client.chat_postMessage(
                    channel=channel_id,
                    text=f"Hi <@{user}>! There are {n_content} contents.",
                    thread_ts=thread_ts
                )

        slack_token = config.SLACK_TOKEN
        rtm_client = slack.RTMClient(token=slack_token)
        rtm_client.start()
        print("I'm listening")
