import asyncio
import difflib
from django.core import management
from django.utils.timezone import now
from eztaskmanager.services.logger import LoggerEnabledCommand
from project.webapp.models import Content


class Command(LoggerEnabledCommand):
    help = "Verify content of specified URI's ids or all"

    def add_arguments(self, parser):
        parser.add_argument('ids', nargs='*', type=int)
        parser.add_argument('--dry-run', action='store_true', dest='dryrun', default=False,
                            help='Execute a dry run: no db is written, no notification sent.')
        parser.add_argument('--content', action='store_true', dest='showmeat', default=False,
                            help='Show extracted text')
        parser.add_argument('--diff', action='store_true', dest='showdiff', default=False,
                            help='Show diff code.')
        parser.add_argument('--offset', type=int, dest='offset', default=0, help='Force offset <> 0')
        parser.add_argument('--limit', type=int, dest='limit', default=0, help='Force limit <> 0')
        parser.add_argument('--notify', action='store_true', dest='notify', default=False,
                            help='Notify changes to registered recipients or channels')
        parser.add_argument('--only-errors', action='store_true', dest='only_errors', default=False,
                            help='Verify only contents that have error status')
        parser.add_argument('--notification-method', dest='notification_method', default='slack',
                            help='What method to use for notification: slack|email|both')
        parser.add_argument('--max-concurrency', type=int, dest='max_concurrency', default=5,
                            help='Limit the number of concurrent verifications')

    async def verify_content(self, content, options, cnt, total, semaphore):
        """Funzione asincrona per verificare il contenuto."""
        async with semaphore:
            self.logger.info(f"Analyzing {content.title} {content.id}")
            err_msg = ''
            try:
                await asyncio.to_thread(content.verify)
            except IOError:
                err_msg = f"Url non leggibile: {content.url}"
            except Exception as e:
                err_msg = f"Errore sconosciuto: {e}"
            finally:
                if err_msg:
                    if not options['dryrun']:
                        content.verification_status = Content.STATUS_ERROR
                        content.verification_error = err_msg
                        content.verified_at = now()
                        await asyncio.to_thread(content.save)
                    self.logger.warning(
                        f"{cnt + 1}/{total} - {err_msg} while processing {content.title} (id: {content.id})")
                else:
                    status = content.verification_error if content.verification_error else (
                        content.get_verification_status_display().upper())
                    self.logger.info(f"{cnt + 1}/{total} - {content.title} (id: {content.id}) - {status}")
                    if options['showmeat']:
                        self.logger.info(f"Contenuto significativo: {content.get_live_content()}")
                    if options['showdiff']:
                        live = content.get_live_content().splitlines(1)
                        stored = content.meat.splitlines(1)
                        diff = difflib.ndiff(live, stored)
                        self.logger.info("".join(diff))

    async def async_handle(self, options):
        offset = options['offset']
        limit = options['limit']
        ids = options.get('ids', [])
        only_errors = options['only_errors']
        max_concurrency = options.get('max_concurrency', 5)

        semaphore = asyncio.Semaphore(max_concurrency)

        contents = Content.objects.filter(is_verification_enabled=True).order_by('id')
        if only_errors:
            contents = contents.filter(verification_status=Content.STATUS_ERROR)

        if not ids:
            contents = contents[offset:(offset + limit)] if limit > 0 else contents[offset:]
        else:
            contents = contents.filter(id__in=ids)

        if not contents.exists():
            self.logger.info("No content to check this time")
            return

        tasks = [self.verify_content(content, options, cnt, len(contents), semaphore)
                 for cnt, content in enumerate(contents)]
        await asyncio.gather(*tasks)

        if options['notify'] and not options['dryrun']:
            verbosity = int(options.get("verbosity", 1))
            management.call_command(
                'notify',
                verbosity=verbosity,
                notification_method=options['notification_method'],
                stdout=self.stdout,
            )

    def handle(self, *args, **options):
        asyncio.run(self.async_handle(options))
