import slack
from constance import config
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend
from eztaskmanager.services.logger import LoggerEnabledCommand

from project.webapp.models import Content, Recipient


class Command(LoggerEnabledCommand):
    help = 'Notify variations, if necessary, to all recipients'

    def add_arguments(self, parser):
        parser.add_argument(
            '--dryrun',
            action='store_true',
            dest='dryrun',
            default=False,
            help='Execute a dry run: no db is written.'
        )
        parser.add_argument(
            "--notification-method",
            dest="notification_method",
            default="slack",
            help="How notifications are sent. [slack|email|both]"
        )

    def handle(self, *args, **options):
        """

        :param args:
        :param options:
        :return:
        """
        contents = Content.objects.filter(is_verification_enabled=True)

        context = {
            'config': config,
            'domain': Site.objects.get_current().domain,
            'n_analysed_contents': contents.count(),
            'modified_contents': contents.filter(verification_status=Content.STATUS_CHANGED),
            'failed_contents': contents.filter(verification_status=Content.STATUS_ERROR),
            'n_modified_contents': contents.filter(verification_status=Content.STATUS_CHANGED).count(),
            'n_failed_contents': contents.filter(verification_status=Content.STATUS_ERROR).count(),
        }

        summary_txt = \
            f"Trovati {context['n_modified_contents']} siti con variazioni e " \
            f"{context['n_failed_contents']} con errori."

        if options['notification_method'] == 'slack':
            self.handle_slack(context=context, summary_txt=summary_txt, **options)
        elif options['notification_method'] == 'email':
            self.handle_email(context=context, summary_txt=summary_txt, **options)
        else:
            self.handle_slack(context=context, summary_txt=summary_txt, **options)
            self.handle_email(context=context, summary_txt=summary_txt, **options)

    def handle_slack(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        context = kwargs.get('context')
        summary_txt = kwargs.get('summary_txt', '')

        linked_txt = \
            f"Trovati *{context['n_modified_contents']}* " \
            f"<http://{context['domain']}/admin/webapp/content/?verification_status__exact=1|siti con variazioni> e " \
            f"*{context['n_failed_contents']}* " \
            f"<http://{context['domain']}/admin/webapp/content/?verification_status__exact=2|siti con errori>."

        msg_blocks = [
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": f"Verificafonti ha analizzato *{context['n_analysed_contents']}* siti."
                    }
                ]
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": linked_txt
                }
            },
        ]

        client = slack.WebClient(token=config.SLACK_TOKEN)
        client.chat_postMessage(
            channel=config.SLACK_CHANNEL,
            text=summary_txt,
            blocks=msg_blocks
        )
        self.logger.info(summary_txt)

    def handle_email(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        context = kwargs.get('context')

        if context['n_modified_contents'] + context['n_failed_contents'] > 0:
            msg_txt = kwargs.get('summary_txt')
            msg_html = \
                f"Trovati " \
                f"<a href=\"http://{context['domain']}/admin/webapp/content/?verification_status__exact=1\">" \
                f"{context['n_modified_contents']} siti con variazioni </a> e " \
                f"<a href=\"http://{context['domain']}/admin/webapp/content/?verification_status__exact=2\">" \
                f"{context['n_failed_contents']} siti con errori</a>."

            recipients = []
            for recipient in Recipient.objects.all():
                recipients.append(recipient.email)
            self.logger.info("Sending emails to: " + "; ".join(recipients))

            if kwargs.get('dryrun', False) is False:
                try:
                    smtp_backend = EmailBackend(
                        host=config.SMTP_HOST,
                        port=config.SMTP_PORT,
                        use_tls=config.SMTP_USE_TLS,
                        username=config.SMTP_USERNAME,
                        password=config.SMTP_PASSWORD,
                    )
                    # smtp_connection = mail.get_connection(backend=smtp_backend)
                    subject = config.EMAIL_SUBJECT_PREFIX
                    with smtp_backend as smtp_connection:
                        msg = EmailMultiAlternatives(
                            subject, msg_txt,
                            from_email=config.EMAIL_FROM,
                            bcc=recipients,
                            connection=smtp_connection
                        )
                        msg.attach_alternative(msg_html, "text/html")
                        msg.send()
                    self.logger.info("Emails sent.")
                except Exception as e:
                    self.logger.error("error sending email: {0}".format(e))
            else:
                self.logger.debug("will not send a bit; it's a dry run!")
        else:
            self.logger.info("No changes detected. Smile!")
