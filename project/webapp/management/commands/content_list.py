# coding=utf-8
from eztaskmanager.services.logger import LoggerEnabledCommand

from project.webapp.models import Content


class Command(LoggerEnabledCommand):
    help = 'List all contents in the DB ID, title and verification status'

    def handle(self, *args, **options):
        """Contains the command logic.
        Launch the scrapy crawl histadmin subprocess and log the output.

        :param args:
        :param options:
        :return:
        """
        for content in Content.objects.all():
            self.logger.info("{0.id} - \"{0.title}\" ({0.verification_status})".format(content))
