from django_rq import job


@job
def verify_contents(contents):
    result = []
    for obj in contents:
        result.append(
            (obj.url, obj.verify())
        )
    return result


@job
def update_contents(contents):
    result = []
    for obj in contents:
        res = obj.url, obj.update()
        result.append(res)
    return result


def say_hello(name="world"):
    msg = f"Hello, {name}!"
    print(msg)
    return msg
