import threading
import functools


class TimeoutError(Exception):
    """Eccezione personalizzata per timeout"""
    pass


def timeout_decorator(seconds=10):
    seconds = int(seconds)

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            result_container = [None]
            exception_container = [None]

            def target():
                try:
                    result_container[0] = func(*args, **kwargs)
                except Exception as e:
                    exception_container[0] = e

            thread = threading.Thread(target=target)
            thread.start()
            thread.join(seconds)

            if thread.is_alive():
                raise TimeoutError(f"Timeout! L'esecuzione ha superato {seconds} secondi.")

            if exception_container[0]:
                raise exception_container[0]

            return result_container[0]

        return wrapper
    return decorator
