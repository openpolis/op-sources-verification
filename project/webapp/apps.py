from django.apps import AppConfig


class WebappConfig(AppConfig):
    name = 'project.webapp'
