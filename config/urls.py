# coding=utf-8
from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.urls import path
from django.views import defaults as default_views
from django.views.generic import RedirectView

from project.webapp.views import diff, signal

admin.autodiscover()

urlpatterns = [
    path("", RedirectView.as_view(pattern_name='admin:index'), name="home"),
    path("favicon.ico", RedirectView.as_view(url='/static/images/favicon.ico')),
    path(settings.ADMIN_URL, admin.site.urls),
    path("diff/<int:content_id>/", diff, name='diff'),
    path("signal/<int:content_id>/", signal, name='signal'),
]

urlpatterns += [
    path('django-rq/', include('django_rq.urls')),
    path('eztaskmanager/', include('eztaskmanager.urls'))

]


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
