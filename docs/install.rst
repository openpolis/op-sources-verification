Install
=======

Clone this repository, then enter in the cloned folder.

Copy ``config/sample/.env`` into ``config/``, and change values according to your case.


.. code-block:: bash

    $ pip install -r requirements/development.txt
    $ createdb -Upostgres DB_NAME
    $ python project/manage.py migrate
    $ python project/manage.py runserver


To launch an uWSGI server with a process to handle asynchronous tasks:

.. code-block:: bash

    uwsgi --check-static=./resources --http=:8000 --master \
      --module=wsgi --callable=application \
      --pythonpath=/Users/gu/Workspace/op-sources-verification \
      --processes=1 --spooler=./resources/uwsgi-spooler --spooler-processes=1