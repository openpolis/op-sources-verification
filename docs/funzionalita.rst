Funzionalità
============

Uno script periodico, lanciato in automatico, controlla un elenco di **contenuti** sul web.
Quando uno di questi contenuti **cambia** rispetto a quanto immagazzinato all'ultima visita,
lo status del contenuto cambia a ``CHANGED``.

Se durante il controllo il contenuto non è raggiungibile, o vengono generati degli errori, lo status del contenuto
viene posto a ``ERROR``.

Se il contenuto non contiene modifiche, rispetto all'ultima visita effettuata, lo status del contenuto
è posto a ``UNCHANGED``.

Un secondo script periodico, invia una **notifica**, in cui sono riassunti i risultati complessivi della verifica,
quanti siti cambiati, quanti errori.
Nel messaggio di notifica sono presenti link a tutti i contenuti cambiati e a tutti quelli che hanno generato un errore.

Un **contenuto** è parte di una pagina web, identificata da una URL e un'**espressione xpath**, o un **selettore CSS**.

Una **notifica** è un messagio via email, inviato a dei recipients, e/o un messaggio inviato a un canale slack.
I recipients possono essere aggiunti al modello, mentre il canale slack è definito a livello di configurazione.

Gli script sono automatizzati attraverso un meccanismo che ne permetta la gestione a utenti non-developer,
attraverso un'interfaccia utente. Le funzionalità del sistema di gestione task sono quelle del
pacchetto ``django-uwsgi-taskmanager``.

Gli utenti operatori possono accedere all'interfaccia di gestione (django admin), con diversi permessi.

  - amministratori:

    - gestiscono l'elenco dei contenuti da verificare,
    - gestiscono l'elenco di tipologie di organizzazioni per i quali i contenuti valgono
    - gestiscono utenti e permessi,
    - gestiscono recipients delle notifiche via email (se configurate)
    - gestiscono i task di verifica e di invio notifiche del task manager

  - operatori:

    - gestiscono l'elenco dei contenuti da verificare
    - gestiscono l'elenco di tipologie di organizzazioni per i quali i contenuti valgono


La gestione dei contenuti da verificare consiste in queste operazioni:

  - visualizzazione elenco
  - ricerca per campo di contenuto, titolo, url
  - filtro per status, per data di ultima verifica, per tipologia di organizzazione
  - ordinamento per data di ultima verifica e url
  - operazioni CRUD
  - visualizzazione delle differenze tra il contenuto *live* e il contenuto in cache
  - lancio di azioni custom, per singolo contenuto:

    - lancio di una verifica di un singolo contenuto, tramite custom action
    - abilitazione/disabilitazione del contenuto alle verifiche
    - segnalazione che i dati su OP sono stati aggiornati
    - segnalazione di un errore (manuale)
    - reset - riporta lo status del contenuto alla condizione iniziale:

      - status nullo
      - data ultima verifica nulla
      - messaggio di errore nullo
      - contenuto in memoria nullo

Un contenuto è definito da:

  - una denominazione, che ne permette il riconoscimento visivo nell'elenco
  - una tipologia di organizzazione (da elenco, per aiutare a visualizzare solo alcuni contenuti)
  - una URL
  - un'espressione XPATH o un selettore CSS (da decidere o con un prefisso, in modo da permettere entrambi)
  - uno stato
  - un flag di abilitazione alla verifica (per spegnere alcune verifiche, se problematiche)
  - l'orario dell'ultima verifica
  - il messaggio di errore, se c'è stato, dell'ultima verifica

Un recipient è definito da:

  - un nome
  - un indirizzo email

Una tipologia di organizzazione è definita da:

  - un nome