Deploy
========

Il progetto è multi-tenancy, ottenuta tramite il deploy su diversi stack docker.

Questo permette il riuso di tutto il codice, mentre i dati, contenuti nei volumi persistenti sono mantenuti distinti.

Attraverso l'uso di variabili d'ambiente, il file ``docker-compose.yml`` è unico.

Le variabili d'ambiente sono lette in fase di inizializzazione del docker-machine env:


.. code-block::

    dmctl set depp-prod-1 config/.dcenv_oc
    dc up


La variabile ``COMPOSE_PROJECT_NAME`` contiene il contesto di esecuzione, e fa in modo che i diversi servizi siano destinati a differenti istanze.



Setup nuova istanza
-------------------

Immaginiamo di voler creare una nuova istanza, per il cliente ACME, sul server `depp-prod-1`.

È necessario aver configurato traefik in modo che sia in grado di accettare label dai container docker.
In particolare, le istanze di opsv, necessitano di label per il redirect da http e https definite nel middleware,
a livello di container, dato che il traefik è definito globalmente e gestisce anche richieste http per un altro progetto.

Il server ``depp-prod-1`` è già configurato in questo modo.

Creiamo il file di configurazione ``config/.dcenv_acme``

.. code-block::

    DEBUG=on
    DEBUG_TOOLBAR_INTERNAL_IPS=127.0.0.1,172.21.0.10,172.23.2.100
    POSTGRES_DB=opsv
    POSTGRES_USER=USER
    POSTGRES_PASS=PASS
    POSTGRES_DEFAULT_PASSWORD=PASSWORD
    SECRET_KEY="VERY_SECRET_KEY_GOES_HERE"
    ALLOWED_HOSTS=instance_name.opsv.openpolis.io
    DEFAULT_FROM_EMAIL=noreply@openpolis.it
    SERVER_EMAIL=noreply@openpolis.it
    ADMIN_EMAIL=lab@openpolis.it
    LETSENCRYPT_EMAIL=guglielmo@openpolis.it
    DOMAIN=instance_name.opsv.openpolis.io
    CI_COMMIT_SHA=""
    EMAIL_HOST=email-smtp.eu-west-1.amazonaws.com
    EMAIL_HOST_PORT=587
    EMAIL_HOST_USE_TLS=True
    EMAIL_HOST_USER=AWS_KEY
    EMAIL_HOST_PASSWORD=AWS_SECRET
    CERT_RESOLVER=prod-resolver
    COMPOSE_PROJECT_NAME=instance_opsv


Definiamo il puntamento per il dominio `instance.opsv.openpolis.io` all'IP del server selezionato.

A questo punto, avviamo l'istanza

.. code-block::

    dmctl set depp-prod-1 config/.dcenv_instance
    dc up -d

Il DB viene creato, ma i contenuti sono errati. È necessario effettuare questi passi:

1. Cambiare la password all'utente op,
   secondo quanto scritto nel file di configurazione (``POSTGRES_PASS``).

.. code-block::

   dc exec postgres psql -Uop opsv
   # alter user op password 'op_pass';

2. Distruggere il database e ricrearlo

.. code-block::

    dc exec postgres dropdb -Uop opsv
    dc exec postgres createdb -Uop opsv

3. Lanciare i management task per le migrazioni, il collectstatic e la creazione del superuser:

.. code-block::

    dc exec web python manage.py migrate
    dc exec web python manage.py createsuperuser
    dc exec web python manage.py collectstatic --noinput


A questo punto l'istanza è pronta ad accettare il login del superuser.